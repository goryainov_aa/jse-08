package ru.goryainov.tm;

import ru.goryainov.tm.dao.ProjectDAO;
import ru.goryainov.tm.dao.TaskDAO;
import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.Task;

import java.util.Scanner;

import static ru.goryainov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение с поддержкой аргументов запуска
 */

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    static {
        projectDAO.create("demo project 1");
        projectDAO.create("demo project 2");
        taskDAO.create("test task 1");
        taskDAO.create("test task 2");
    }

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */
    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
        }
    }

    /**
     * Запуск приложения
     *
     * @param args массив параметров запуска
     */
    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Запуск приложения
     *
     * @param param параметр запуска
     */
    private static int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_VIEW_BY_ID:
                return viewProjectById();
            case PROJECT_VIEW_BY_INDEX:
                return viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX:
                return updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return updateProjectById();
            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();
            case TASK_VIEW_BY_INDEX:
                return viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return viewTaskById();
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX:
                return updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return updateTaskById();
            default:
                return displayError();
        }
    }

    /**
     * Вывод сведений о разработчике
     */
    private static int displayAbout() {
        System.out.println("Andrey Goryainov");
        System.out.println("goryainov_aa@nlmk.ru");
        return 0;
    }

    /**
     * Вывод версии
     */
    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Вывод списка возможных параметров запуска
     */
    private static int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - display list of projects.");
        System.out.println("project-create - create new project by name.");
        System.out.println("project-clear - remove all projects.");
        System.out.println("project-view-by-index - display information about project by index.");
        System.out.println("project-view-by-id - display information about project by id.");
        System.out.println("project-remove-by-name - remove project by name.");
        System.out.println("project-remove-by-index - remove project by index.");
        System.out.println("project-remove-by-id - remove project by id.");
        System.out.println("project-update-by-index - update project by index.");
        System.out.println("project-update-by-id - update project by id.");
        System.out.println();
        System.out.println("task-list - display list of tasks.");
        System.out.println("task-create - create new task by name.");
        System.out.println("task-clear - remove all tasks.");
        System.out.println("task-view-by-index - display information about task by index.");
        System.out.println("task-view-by-id - display information about task by id.");
        System.out.println("task-remove-by-name - remove task by name.");
        System.out.println("task-remove-by-index - remove task by index.");
        System.out.println("task-remove-by-id - remove task by id.");
        System.out.println("task-update-by-index - update task by index.");
        System.out.println("task-update-by-id - update task by id.");
        return 0;
    }

    /**
     * Вывод приветствия
     */
    private static void displayWelcome() {
        System.out.println("-= WELCOME TO TASK MANAGER =-");
    }

    /**
     * Вывод ошибки при вводе параметра запуска не из списка
     */
    private static int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    /**
     * Вывод завершения приложения
     */
    private static int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    /**
     * Вывод создания проекта
     */
    private static int createProject() {
        System.out.println("[Create project]");
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanner.nextLine();
        projectDAO.create(name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение проекта по индексу
     */
    private static int updateProjectByIndex() {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project index:]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectDAO.findByIndex(index);
        if (project == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanner.nextLine();
        projectDAO.update(project.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение проекта по идентификатору
     */
    private static int updateProjectById() {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project id:]");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectDAO.findById(id);
        if (project == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanner.nextLine();
        projectDAO.update(project.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по имени
     */
    private static int removeProjectByName() {
        System.out.println("[Remove project by name]");
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по идентификатору
     */
    private static int removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("[Please, enter project id:]");
        final long id = scanner.nextLong();
        final Project project = projectDAO.removeById(id);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по индексу
     */
    private static int removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("[Please, enter project index:]");
        final int index = scanner.nextInt() - 1;
        final Project project = projectDAO.removeByIndex(index);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод очистки проекта
     */
    private static int clearProject() {
        System.out.println("[Clear project]");
        projectDAO.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр проекта по индексу
     */
    private static int viewProjectByIndex() {
        System.out.println("Enter, project index: ");
        final int index = scanner.nextInt() - 1;
        final Project project = projectDAO.findByIndex(index);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по идентификатору
     */
    private static int viewProjectById() {
        System.out.println("Enter, project id: ");
        final Long id = scanner.nextLong();
        final Project project = projectDAO.findById(id);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр списка проектов
     */
    private static void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[View project]");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка проектов
     */
    private static int listProject() {
        System.out.println("[List project]");
        int index = 1;
        for (final Project project : projectDAO.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод создания задачи
     */
    private static int createTask() {
        System.out.println("[Create task]");
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        taskDAO.create(name);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение задачи по индексу
     */
    private static int updateTaskByIndex() {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task index:]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskDAO.findByIndex(index);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanner.nextLine();
        taskDAO.update(task.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение задачи по идентификатору
     */
    private static int updateTaskById() {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task id:]");
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = taskDAO.findById(id);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanner.nextLine();
        taskDAO.update(task.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистка задачи
     */
    private static int clearTask() {
        System.out.println("[Clear task]");
        taskDAO.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по имени
     */
    private static int removeTaskByName() {
        System.out.println("[Remove task by name]");
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по идентификатору
     */
    private static int removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("[Please, enter task id:]");
        final long id = scanner.nextLong();
        final Task task = taskDAO.removeById(id);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по индексу
     */
    private static int removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("[Please, enter task index:]");
        final int index = scanner.nextInt() - 1;
        final Task task = taskDAO.removeByIndex(index);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр задачи по индексу
     */
    private static int viewTaskByIndex() {
        System.out.println("Enter, task index: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskDAO.findByIndex(index);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр задачи по идентификатору
     */
    private static int viewTaskById() {
        System.out.println("Enter, task id: ");
        final Long id = scanner.nextLong();
        final Task task = taskDAO.findById(id);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр списка задач
     */
    private static void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[View task]");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка задач
     */
    private static int listTask() {
        System.out.println("[List task]");
        int index = 1;
        for (final Task task : taskDAO.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[Ok]");
        return 0;
    }
}
